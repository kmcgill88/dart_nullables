## 0.2.0
- Added `firstWhereOrNull` to Iterable.
- Bumped min SDK from `2.12.0` to `2.17.0`
- Enable Dart 3; set SDK to `sdk: '>=2.17.0 <4.0.0'`

## 0.1.0
- Added `tryFirst` to Iterable.

## 0.0.0
- Initial version.
