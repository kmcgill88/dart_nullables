extension Nullables<T> on T? {
  bool get isNull => this == null;

  bool get isNotNull => !isNull;

  T? get orNull => this;

  T or(T backup) => this ?? backup;

  Nullable<T> get toNullable => Nullable(this);
}

extension StringX on String? {
  String get orEmptyString => this ?? '';

  bool get isNotNullOrEmpty => isNotNull && this!.isNotEmpty;

  bool get isNullOrEmpty => isNull || this!.isEmpty;
}

extension IterableX<T> on Iterable<T>? {
  bool get isNotNullOrEmpty => isNotNull && this!.isNotEmpty;

  bool get isNullOrEmpty => isNull || this!.isEmpty;

  T? get tryFirst => isNotNullOrEmpty ? this!.first : null;

  T? firstWhereOrNull(bool Function(T element) test) {
    if (isNull) return null;
    return this!.firstWhereOrNull(test);
  }
}

extension NonNullIterableX<T> on Iterable<T> {
  T? firstWhereOrNull(bool Function(T element) test) {
    for (final element in this) {
      if (test(element)) return element;
    }
    return null;
  }
}

extension NullableX<T> on Nullable<T>? {
  T? copyNullableOr(T? backup) => isNotNull ? this?.value : backup;
}

class Nullable<T> {
  final T? value;

  const Nullable(this.value);

  T? get orNull => value;
}
