# mcnullable_extensions
[![pipeline status](https://gitlab.com/kmcgill88/mcnullable_extensions/badges/main/pipeline.svg)](https://gitlab.com/kmcgill88/mcnullable_extensions/-/commits/main)
[![MIT license](https://img.shields.io/badge/License-MIT-blue.svg)](https://gitlab.com/kmcgill88/mcnullable_extensions/-/blob/main/LICENSE)
[![PRs Welcome](https://img.shields.io/badge/PRs-welcome-brightgreen.svg?style=flat-square)](https://gitlab.com/kmcgill88/mcnullable_extensions)

A collection of extension methods to do less `foo != null` and more `foo.isNotNull`.


## Usage - non-exhaustive list
### T? 
- `isNull`
- `isNotNull`
- `or('backup')`

```dart
// isNull
expect(null.isNull, isTrue);

String? string;
expect(string.isNull, isTrue);

int? i;
expect(i.isNull, isTrue);

DumbCat? dumbCat;
expect(dumbCat.isNull, isTrue);

// isNotNull
num? n;
expect(n.isNotNull, isFalse);
num? n2 = 9;
expect(n2.isNotNull, isTrue);

DumbCat? dumbCat;
expect(dumbCat.isNotNull, isFalse);

// or
String? string;
expect(string.or('backup'), 'backup');
```

### `String?`
- `isNullOrEmpty`
- `isNotNullOrEmpty`
- `orEmptyString`

```dart
// isNullOrEmpty
String? string;
expect(string.isNullOrEmpty, isTrue);

string = '';
expect(string.isNullOrEmpty, isTrue);

// orEmptyString
String? string;
expect(string.orEmptyString, '');

// isNotNullOrEmpty
String? string;
expect(string.isNotNullOrEmpty, isFalse);
expect(''.isNotNullOrEmpty, isFalse);

expect('words'.isNotNullOrEmpty, isTrue);
string = 'Hello Dart!';
expect(string.isNotNullOrEmpty, isTrue);
```

