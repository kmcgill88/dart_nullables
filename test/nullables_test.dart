import 'package:mcnullable_extensions/mcnullable_extensions.dart';
import 'package:test/test.dart';

class DumbCat {
  DumbCat.name(this.name);

  String? name;
}

void main() {
  group('Nullable T', () {
    test('or', () {
      String? string;
      expect(string.or('backup'), 'backup');

      DumbCat? dumbCat;
      final backup = DumbCat.name('backup');
      expect(dumbCat.or(backup), backup);
      expect(dumbCat.or(backup).name.or('a'), 'backup');
    });

    test('isNull', () {
      expect(null.isNull, isTrue);

      String? string;
      expect(string.isNull, isTrue);

      int? i;
      expect(i.isNull, isTrue);

      double? d;
      expect(d.isNull, isTrue);

      num? n;
      expect(n.isNull, isTrue);

      DumbCat? dumbCat;
      expect(dumbCat.isNull, isTrue);
    });

    test('isNotNull', () {
      expect(null.isNotNull, isFalse);

      String? string;
      expect(string.isNotNull, isFalse);
      String? string2 = 'Hello Dart!';
      expect(string2.isNotNull, isTrue);

      int? i;
      expect(i.isNotNull, isFalse);

      double? d;
      expect(d.isNotNull, isFalse);

      num? n;
      expect(n.isNotNull, isFalse);
      num? n2 = 9;
      expect(n2.isNotNull, isTrue);

      DumbCat? dumbCat;
      expect(dumbCat.isNotNull, isFalse);
    });
  });

  group('StringX', () {
    test('isNullOrEmpty', () {
      String? string;
      expect(string.isNullOrEmpty, isTrue);

      string = '';
      expect(string.isNullOrEmpty, isTrue);
    });

    test('orEmptyString', () {
      String? string;
      expect(string.orEmptyString, '');
    });

    test('isNotNullOrEmpty', () {
      String? string;
      expect(string.isNotNullOrEmpty, isFalse);
      expect(''.isNotNullOrEmpty, isFalse);

      expect('words'.isNotNullOrEmpty, isTrue);
      string = 'Hello Dart!';
      expect(string.isNotNullOrEmpty, isTrue);
    });
  });

  group('doubleX', () {
    test('isNull', () {
      double? doubleVar;
      expect(doubleVar.isNull, isTrue);

      doubleVar = 0;
      expect(doubleVar.isNull, isFalse);
    });

    test('or', () {
      double? doubleVar;
      expect(doubleVar.or(0), 0);

      doubleVar = 5;
      expect(doubleVar.or(0), 5);

      doubleVar = null;
      expect(doubleVar.or(-1), -1);
    });
  });

  group('IterableX', () {
    test('isNotNullOrEmpty', () {
      List<String>? items = ['one'];
      expect(items.isNotNullOrEmpty, isTrue);

      List<String>? items2;
      expect(items2.isNotNullOrEmpty, isFalse);
    });

    test('isNullOrEmpty', () {
      List<String>? items = ['one'];
      expect(items.isNullOrEmpty, isFalse);

      List<String>? items3 = [];
      expect(items3.isNullOrEmpty, isTrue);

      List<String>? items2;
      expect(items2.isNullOrEmpty, isTrue);

      Set<String>? items4;
      expect(items4.isNullOrEmpty, isTrue);
    });

    test('tryFirst', () {
      List<String>? items = ['one'];
      expect(items.tryFirst, 'one');

      List<String>? items3 = [];
      expect(items3.tryFirst, isNull);

      List<String>? items2;
      expect(items2.tryFirst, isNull);

      Set<String>? items4;
      expect(items4.tryFirst, isNull);
    });

    test('firstWhereOrNull', () {
      List<String>? items = ['one'];
      expect(
        items.firstWhereOrNull((element) => element == 'one'),
        'one',
      );
      expect(
        items.firstWhereOrNull((element) => element == 'two'),
        isNull,
      );

      List<String>? items3;
      items3 = [];
      expect(
        items3.firstWhereOrNull((element) => element == 'one'),
        isNull,
      );

      Set<String>? items4;
      expect(
        items4.firstWhereOrNull((element) => element == 'one'),
        isNull,
      );
    });
  });

  group('NonNullIterableX', () {
    test('firstWhereOrNull', () {
      List<String> items = ['one'];
      expect(
        items.firstWhereOrNull((element) => element == 'one'),
        'one',
      );
      expect(
        items.firstWhereOrNull((element) => element == 'two'),
        isNull,
      );

      List<String> items3 = [];
      expect(
        items3.firstWhereOrNull((element) => element == 'one'),
        isNull,
      );

      Set<String> items4 = {};
      expect(
        items4.firstWhereOrNull((element) => element == 'one'),
        isNull,
      );
    });
  });
}
